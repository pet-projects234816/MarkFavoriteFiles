﻿using System;
using Serilog;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarkFavoriteFiles.Config
{
    /// <summary>
    /// Класс проверок и тестов для параметров и проч.
    /// </summary>
    class TestsNCheks
    {
        /*------------------ Поля и Свойства -------------------*/
        /// <summary>
        /// Поле - используется для метода RunAllTestByDefault() - общий статус всех тестов и проверок
        /// </summary>
        private Settings.Status _testStatus;

        /// <summary>
        /// Переменная - внутренняя: временная, для хранения статуса каждого теста или проверки
        /// </summary>
        private Settings.Status _tmpStatus;

        /// <summary>
        /// Переменная - внутренняя: временная, для хранения описания для каждого теста
        /// </summary>
        private string _tmpTestDescription;

        /// <summary>
        /// Переменная - внутренняя: временная, для хранения комментария для статуса
        /// </summary>
        private string _tmpСommentForStatus;


        /*----------------------------------------------------*/

        /*----------------------------------------------------*/
        /// <summary>
        /// Метод: вывести на экран наименования и значения параметров
        /// </summary>  
        public void ShowOptionsSettings()
        {
            Log.Information("\n========= Значение параметров =========\n" +
                "Входные параметры:\n" +
                "     SourceRoot:\t\t{SourceRoot}\n" +
                "     TargetRoot:\t\t{TargetRoot}\n" +
                "     PrefixLike:\t\t{PrefixLike}\n" +
                "     PrefixDislike:\t\t{PrefixDislike}\n" +
                "     FilePattern:\t\t{FilePattern}\n" +
                "     IncrementDir:\t\t{IncrementDir}\n" +
                "     Verbose:\t\t\t{Verbose}\n" +
                "\nСистемные параметры:\n" +
                "     Status:\t\t\t{StatusEnum}\n" +
                "     PreviewChanges:\t\t{PreviewChanges}\n" +
                "     PostfixForNewFilesFolder:\t{PostfixForNewFilesFolder}\n" +

                "=======================================\n\n",
                Settings.SourceRoot,
                Settings.TargetRoot,
                Settings.PrefixLike,
                Settings.PrefixDislike,
                Settings.FilePattern,
                Settings.IncrementDir,
                Settings.Verbose,
                "{" + Settings.Status.Success.ToString() + ", " +
                      Settings.Status.Warning.ToString() + ", " +
                      Settings.Status.Error.ToString() + ", " + 
                      Settings.Status.Indefined.ToString() + "}",
                Settings.PreviewChanges,
                Settings.PostfixForNewFilesFolder
            );

        }

        /*----------------------------------------------------*/






        /*--- Метод MessageInSinksByTestStatus и его перегрузки ---*/

        /// <summary>
        /// Метод - выводит данные теста в поток: статус теста 
        /// </summary>
        public void MessageInSinksByTestStatus(Settings.Status status)
        {
            this.MessageInSinksByTestStatus(status, "", "");
        }

        /// <summary>
        /// Метод - выводит данные теста в поток: определение теста + статус теста 
        /// </summary>
        /// <param name="testDescription">Описание теста, которое должно выводиться на экран</param>
        public void MessageInSinksByTestStatus(Settings.Status status, string testDescription)
        {
            this.MessageInSinksByTestStatus(status, testDescription, "");
        }


        /// <summary>
        /// Метод - выводит данные теста в поток: определение теста + статус теста + комментарий теста
        /// </summary>
        /// <param name="testDescription">Описание теста, которое должно выводиться на экран</param>
        /// <param name="commentForStatus">Комментарий для стутуса, если требуется пояснение</param>
        public void MessageInSinksByTestStatus(Settings.Status status, string testDescription, string commentForStatus)
        {
            var _resultText = "\n";

            /* Вывод заголвка теста и его описания */
            if (testDescription != "")
                _resultText +=  "Тест: {testDescription}\n";

            /* Значение статуса теста - выводить всегда */
            _resultText += "-- {Status}.\n";

            /* Вывод описани теста, если требуется */
            if (commentForStatus != "")
                _resultText +=  "-- {commentForStatus}.\n";

            /*  Вывод статусов по цвету и комментарии */
            switch (status)
            {
                case Settings.Status.Success:
                    if (commentForStatus == "")
                        if (testDescription == "") 
                            Log.Information(_resultText, status);
                        else
                            Log.Information(_resultText, testDescription, status);
                    else
                        Log.Information(_resultText, testDescription, status, commentForStatus);
                    break;
                case Settings.Status.Warning:
                    if (commentForStatus == "")
                        if (testDescription == "")
                            Log.Warning(_resultText, status);
                        else
                            Log.Warning(_resultText, testDescription, status);
                    else
                        Log.Warning(_resultText, testDescription, status, commentForStatus);
                    break;

                case Settings.Status.Error:
                    if (commentForStatus == "")
                        if (testDescription == "")
                            Log.Error(_resultText, status);
                        else
                            Log.Error(_resultText, testDescription, status);
                    else
                        Log.Error(_resultText, testDescription, status, commentForStatus);
                    break;
                default:
                    Log.Warning("\n-- {Status} - не ожидаемый статус...\n", status);
                    break;
            }
        }

        /*-----------------------------------------------------------*/






        /// <summary>
        /// Метод - вспомогательный для метода RunAllTestByDefault() - чтобы избежать дублирования,
        ///  определяет приоритет статусов для поля _testStatus
        /// </summary>
        private void StatusPriority(Settings.Status status)
        {
            if (status != Settings.Status.Success)
                if (_testStatus != Settings.Status.Error)
                    _testStatus = status;
        }





        /// <summary>
        /// Метод - запускает все тесты и проверки со значениями по умолчанию
        /// </summary>
        public Settings.Status RunAllTestByDefault(bool verbose)
        {
            // Признак того, что все тесты завершились успешно (со значением Success)
            _testStatus = Settings.Status.Success;

            // Вывод на экран начала тестов
            if (verbose)
                Log.Information("============ Запуск тестов ============\n");



            // Тест : Проверка доступна ли директория источник
            _tmpStatus = DirIsExists(Settings.SourceRoot, out _tmpTestDescription);
            _tmpСommentForStatus = "Проверьте значение входного параметра 's', (\"sourceroot\") - 'Директория источник' ";

            if (verbose || _tmpStatus != Settings.Status.Success)
            {
                MessageInSinksByTestStatus(_tmpStatus, _tmpTestDescription,
                    ((_tmpStatus == Settings.Status.Error) ? _tmpСommentForStatus : ""));
            }
            StatusPriority(_tmpStatus);

            // Тест : Проверка доступна ли целевая директория
            _tmpStatus = DirIsExists(Settings.TargetRoot, out _tmpTestDescription);
            _tmpСommentForStatus = "Проверьте значение входного параметра 't', ('targetroot') - 'Целевая директория, в которой помечаются файлы.'";

            if (verbose || _tmpStatus != Settings.Status.Success)
            {
                MessageInSinksByTestStatus(_tmpStatus, _tmpTestDescription,
                    ((_tmpStatus == Settings.Status.Error) ? _tmpСommentForStatus : ""));
            }
            StatusPriority(_tmpStatus);



            // Вывод на экран окончания тестов
            if (verbose)
                Log.Information("=======================================\n");

            // Вернуть общий статус для всех тестов
            return _testStatus;
        }



        /*--- Метод DirIsExists + его перезагрузки -------------*/
        /// <summary>
        /// Метод - проверка: существование директроии
        /// </summary>  
        public Settings.Status DirIsExists(string directoryPath)
        {
            string nullText;
            return this.DirIsExists(directoryPath, out nullText);

        }

        /// <summary>
        /// Метод - проверка: существование директроии + вывод описания теста
        /// </summary>  
        public Settings.Status DirIsExists(string directoryPath, out string testDescription)
        {
            _tmpStatus = Directory.Exists(directoryPath) ? Settings.Status.Success : Settings.Status.Error;
            testDescription = "Проверка существования пути к каталогу '" + directoryPath + "':";

            return _tmpStatus;
        }
        /*------------------------------------------------------*/


    }
}
