﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommandLine;
using CommandLine.Text;
using Serilog;

namespace MarkFavoriteFiles.Config
{
    class Options
    {
        

        [Option('s', "sourceroot", Required = true, HelpText = @"Корневая директория источник: D:\LikedMusic")]
        /// <summary>
        /// Корневая директория источник: D:\LikedMusic
        /// </summary>
        public string SourceRoot { get; set; }

        [Option('t', "targetroot", Required = true, HelpText = @"Корневая директория, в которой необходимо отметить файлы: D:\AllMusic")]
        /// <summary>
        /// Корневая директория, в которой необходимо отметить файлы: D:\AllMusic
        /// </summary>
        public string TargetRoot { get; set; }

        private string _prefixLike;

        [Option('l', "prefixlike", HelpText = @"Префикс для переименования файла - понравилось (по умолчанию '+')")]
        /// <summary>
        /// Символ префикса для переименования файла - понравилось
        /// </summary>
        public string PrefixLike {
            get
            {
               // _prefixLike = (_prefixLike == null) ? "" : _prefixLike; -- Данное выражение можно переписать так:
                _prefixLike = _prefixLike ?? string.Empty;
                Boolean notValid = _prefixLike.Intersect(System.IO.Path.GetInvalidFileNameChars()).Any();

                return (_prefixLike == "" || notValid) ? "+ " : _prefixLike;
            }
            set
            {
                _prefixLike = value;
            }
        }


        private string _prefixDislike;

        [Option('d', "prefixDislike", HelpText = @"Префикс для переименования файла - не понравилось (по умолчанию '-')")]
        /// <summary>
        /// Символ префикса для переименования файла - не понравилось
        /// </summary>
        public string PrefixDislike
        {
            get
            {
                _prefixDislike = _prefixDislike ?? string.Empty;
                Boolean notValid = _prefixDislike.Intersect(System.IO.Path.GetInvalidFileNameChars()).Any();

                return (_prefixDislike == "" || notValid) ? "- " : _prefixDislike;
            }
            set
            {
                _prefixDislike = value;
            }
        }




        private string _filePattern;

        [Option('p', "filePattern", HelpText = @"Расширение для файлов, которые будут анализироваться (по умолчанию '*')")]
        /// <summary>
        /// Расширение для файлов, которые будут анализироваться
        /// </summary>
        public string FilePattern
        {
            get
            {
                _filePattern = _filePattern ?? string.Empty;

                return ((_filePattern == string.Empty) ? "*" : _filePattern);
            }
            set
            {
                _filePattern = value;
            }
        }


        [Option('i', "incrementDir", DefaultValue = false, HelpText = "Инкремент дирикторий, с прослушанной музыкой...")]
        public bool IncrementDir { get; set; }


        [Option('v', "verbose", DefaultValue = false, HelpText = "Печать всех сообщений для стандартного вывода...")]
        public bool Verbose { get; set; }


        [ParserState]
        public IParserState LastParserState { get; set; }

        [HelpOption]
        public string GetUsage()
        {
            // this without using CommandLine.Text
            var usage = new StringBuilder();
            return HelpText.AutoBuild(this,
                (HelpText current) => HelpText.DefaultParsingErrorsHandler(this, current));
        }


    }
    
}
