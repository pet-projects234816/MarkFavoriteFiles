﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using Serilog;
using System.Text;
using System.Threading.Tasks;

namespace MarkFavoriteFiles.Config
{
    /// <summary>
    /// Статический класс, в котором храняться методы для использования их в Programs.cs
    /// </summary>
    public static class Methods
    {
        /* ------------------------------------------------------------------------------------------- */
        /*  Методы                                                                                     */
        /* ------------------------------------------------------------------------------------------- */




        /// <summary>
        /// Метод: Получить коллекцию объектов FileStructure 
        /// </summary>
        /// <param name="rootPath">корневая директория для файлов из которых создается колекция</param>
        /// <returns>колекция объектов FileStructure</returns>
        internal static IEnumerable<FileStructure> GetFilesStructureFromPath(string rootPath)
        {
            /* Создание коллекции объектов FileStructure для множества файлов в указанной директории */

            // Получить массив файлов в директории источнике 
            List<string> filesInRoot = new List<string>();
            try
            {
                filesInRoot = Directory.GetFiles(rootPath, Settings.FilePattern, SearchOption.AllDirectories).ToList<string>();

            }
            catch (Exception ex)
            {
                Log.Error("\nЧто то пошло не так... при получении списка в директории:\n{rootPath}\nОшибка исключения:\n{ex}", rootPath, ex);
                filesInRoot.Add(String.Empty);
                Console.WriteLine();
                Environment.Exit(1);
            }


            // Создать массив объектов FileStructure из дирекетории источника
            return filesInRoot.Select(f => new FileStructure(f));

        }


        /// <summary>
        /// Поличить список объктов класса FolderStructure на основании списка объектов класса FilleStructure 
        /// </summary>
        /// <param name="filesStructureFromPath"></param>
        /// <returns></returns>
        internal static IEnumerable<FolderStructure> GetFolderStructureFromPath(IEnumerable<FileStructure> filesStructureFromPath)
        {
            List<string> listDirectoryFile = new List<string>();

            foreach (var fileStructure in filesStructureFromPath)
            {
                if (!listDirectoryFile.Contains(fileStructure.DirectoryFile))
                    listDirectoryFile.Add(fileStructure.DirectoryFile);
            }

            return
                listDirectoryFile.Select(f => new FolderStructure(f));
        }



        /// <summary>
        /// Метод: Позволяет получить список конечных папок - позволяет получить в том числе папки, в которой все файлы удалены
        /// </summary>
        /// <param name="pathRoot">Стартовая директория для анализа структуры папопок</param>
        /// <returns>Список "конченых" папок</returns>
        internal static List<FolderStructure> GetListOfLastFolders(string pathRoot)
        {
            List<string> DirectoriesList = new List<string>(Directory.GetDirectories(pathRoot, "*", SearchOption.AllDirectories));
            List<FolderStructure> FolderList = new List<FolderStructure>();

            foreach (var dir in DirectoriesList)
            {
                FolderList.Add(new FolderStructure(dir));
            }

            return FolderList;
        }






        /// <summary>      
        /// Вспомогательный метод: Возвращает List(FolderStructure) - пересеченное множество "последних каталогов" в TargetRoot
        /// </summary>
        /// <returns></returns>
        internal static List<FolderStructure> GetIntersectedOfTargetNSourceLastFolders()
        {

            // Получить пересечение конечных каталогов для SourceRoot и TargetRoot
            List<FolderStructure> SourceFolderList = Methods.GetListOfLastFolders(Settings.SourceRoot);
            List<FolderStructure> TargetFolderList = Methods.GetListOfLastFolders(Settings.TargetRoot);
            List<FolderStructure> Intersected = new List<FolderStructure>();
            foreach (var folderStructure in TargetFolderList)
            {
                if (SourceFolderList.Exists(element => element.LastFolder.ToUpper() == folderStructure.LastFolder.ToUpper()))
                    Intersected.Add(folderStructure);
            }

            return
                Intersected;
        }





        /// <summary>
        /// Метод: вспомогательный. Переименования множества файлов + log
        /// </summary>
        /// <param name="isLike">true/false - множество (не)понравившихся файлов</param>
        /// <param name="arrFileStructure"> - массив структуры файлов </param>
        /// <param name="renameLog"> результат перименования - текст </param>
        /// <returns> результат переименования - bool </returns>
        internal static bool RenameFilesInFilesStructure(bool isLike, FileStructure[] arrFileStructure, out string renameLog)
        {
            var renameIsSuccess = true;
            renameLog = "";

            string renameResultText;

            string parentPath;
            string parentPathBefore = "d:\\asdfweb34sdfdgs4";

            foreach (FileStructure fileStructure in arrFileStructure)
            {
                parentPath = "\n.." + fileStructure.DiretoryFileWithoutPathRoot;
                if (!fileStructure.RenameFileByPrefix(isLike, out renameResultText))
                    renameIsSuccess = false;
                if (renameResultText != String.Empty)
                {
                    if (parentPath == parentPathBefore)
                    {
                        renameResultText = renameResultText.Replace(parentPath, "");
                    }


                    renameLog += renameResultText;
                }
                parentPathBefore = parentPath;
            }

            return renameIsSuccess;
        }






        /// <summary>
        /// Метод - переименование понравившихся и не понравившихся файлов + инкремент директорий, которые прослушаны.
        /// </summary>
        /// <param name="filesStructureInSourceDir">Массив файловой структуры директории источника</param>
        /// <param name="filesSructureInTargetDir">Массив файловой структуры целевой диреткории</param>
        /// <param name="renameLog">возвращает лог - переименование множества файлов</param>
        /// <returns>true - переименование всех файлов успешно, false - если хотябы переименование одного файла с ошибкой</returns>
        internal static bool LikeDislikeFiles(FileStructure[] filesStructureInSourceDir, FileStructure[] filesSructureInTargetDir, out string renameLog)
        {
            renameLog = "Результат переименования " + (Settings.PreviewChanges ? "(предварительный просмотр)" : String.Empty) + ":\n";
            var renameIsSuccess = true;


            // Уменьшить целевой массив, обрезав элементы, у которых ParentDir не присутствует в массиве источнике
            var i = 0;
            FileStructure[] reducedFilesSructureInTargetDir = new FileStructure[filesSructureInTargetDir.Length];

            foreach (FileStructure fileStructure in filesSructureInTargetDir)
            {
                // Получить "сокращенныей" массив... - сюда не возможно добавить множество FileStructure для тех файлов,
                // которые полностью удалены в одном из каталогов filesStructureInSourceDir.
                // Соответственно чтобы их добавить, необходима проверка на условие reducedFolderList.Contains
                if (/*Array.Exists(filesStructureInSourceDir, element => element.ParentDirectory == fileStructure.ParentDirectory)
                    || */
                    Settings.IntersectedOfTargetNSourceLastFolders
                            .Exists(element => element.LastFolder.ToUpper() == fileStructure.ParentDirectory.ToUpper()))
                {
                    reducedFilesSructureInTargetDir[i++] = new FileStructure(fileStructure.FullPath);
                }
            }


            Array.Resize(ref reducedFilesSructureInTargetDir, i);




            /* Создание массивов объектов - понравившиеся/непонравившиеся файлы/новые файлы */
            FileStructure[] likedFilesSructureInTargetDir = new FileStructure[reducedFilesSructureInTargetDir.Length];
            FileStructure[] dislikedFilesSructureInTargetDir = new FileStructure[reducedFilesSructureInTargetDir.Length];
            FileStructure[] newFilesSructureInInSourceDir = new FileStructure[filesStructureInSourceDir.Length];

            int j, n;
            j = n = i = 0;
            //var listDirectoryFile = new List<FileStructure>();
            // массивы понравишиеся/непонравившиеся
            foreach (FileStructure fileStructure in reducedFilesSructureInTargetDir)
            {
                if (Array.Exists(filesStructureInSourceDir, element => element.ParentDir_FileName == fileStructure.ParentDir_FileName))
                {
                    //listDirectoryFile.Add(new FileStructure(fileStructure.FullPath));
                    likedFilesSructureInTargetDir[i++] = new FileStructure(fileStructure.FullPath);
                }
                else
                    dislikedFilesSructureInTargetDir[j++] = new FileStructure(fileStructure.FullPath);
            }

            Array.Resize(ref likedFilesSructureInTargetDir, i);
            Array.Resize(ref dislikedFilesSructureInTargetDir, j);

            // массив новых файлов
            foreach (FileStructure fileStructure in filesStructureInSourceDir)
            {

                if (!Array.Exists(reducedFilesSructureInTargetDir, element => element.ParentDir_FileName == fileStructure.ParentDir_FileName))
                    newFilesSructureInInSourceDir[n++] = new FileStructure(fileStructure.FullPath);
            }

            Array.Resize(ref newFilesSructureInInSourceDir, n);



            /* Переименование файлов */
            string renameResultText = "";


            // Like
            renameLog += "\n\n\nLike (" + i + ")" + (Settings.PreviewChanges ? " (предварительный просмотр)" : String.Empty) + ":\n----\n";
            if (!RenameFilesInFilesStructure(true, likedFilesSructureInTargetDir, out renameResultText))
            {
                renameIsSuccess = false;
            }
            renameLog += renameResultText;


            // Dislike
            renameLog += "\n\n\n\nDislike (" + j + ")" + (Settings.PreviewChanges ? " (предварительный просмотр)" : String.Empty) + ":\n-------\n";
            if (!RenameFilesInFilesStructure(false, dislikedFilesSructureInTargetDir, out renameResultText))
            {
                renameIsSuccess = false;
            }
            renameLog += renameResultText;


            // NewFiles
            renameLog += "\n\n\n\nNewFiles (" + n + ")" + (Settings.PreviewChanges ? " (предварительный просмотр)" : String.Empty) + ":\n--------\n";
            foreach (FileStructure fileStructure in newFilesSructureInInSourceDir)
            {
                if (!fileStructure.CopyNewFiles(out renameResultText))
                    renameIsSuccess = false;
                if (renameResultText != String.Empty)
                    renameLog += renameResultText + "\n";
            }


            /* Переименование файлов в директории источнике, для возможности повторного запуска */
            renameLog += "\n\n\n\nПереименование в каталоге источнике (" + i + ")"
                + (Settings.PreviewChanges ? " (предварительный просмотр)" : String.Empty)
                + ":\n-----------------------------------\n";
            if (!RenameFilesInFilesStructure(true, filesStructureInSourceDir, out renameResultText))
            {
                renameIsSuccess = false;
            }
            renameLog += renameResultText;


            // false - Если переименование хотябы одного файла было с ошибкой, иначе - true
            return renameIsSuccess;
        }




        /// <summary>
        /// Метод: Инкрементировать каталоги с прослушанными файлами (только для папок, у которых нет дочерних каталогов) (..\01_Folder --> ..\2_Folder) 
        /// </summary>
        /// <param name="listenedLastFolders"> Список FolderStructure </param>
        /// <param name="renameLog">результат переименования в текстовом виде</param>
        /// <returns>true - если переименование всех каталогов успешно, иначе - false</returns>
        internal static bool IncrementListenedLastFolders(List<FolderStructure> listenedLastFolders, out string renameLog)
        {
            bool renameIsSuccess = true;
            renameLog = "";


            var resultBool = true;
            var resultStr = "";
            var parentPathBefore = "d:\\asdfweb34sdfdgs4";

            foreach (var folderStructure in listenedLastFolders)
            {
                resultBool = folderStructure.IncrementLastFolder(out resultStr);
                renameIsSuccess = resultBool ? renameIsSuccess : resultBool;
                // Если переименование происходит в той же папке, что и предыдущая - вырезается родительский путь (это нужно для красивого отображения процесса)
                var parentPath = folderStructure
                    .FullPathDirectory
                    .Replace(folderStructure.LastFolder, "");

                if ((parentPath == parentPathBefore) & (resultStr != ""))
                {
                    var replaceText = "\n" + folderStructure.FullPathDirectory
                                               .Replace(folderStructure.LastFolder, "");
                    resultStr = resultStr.Replace(replaceText, "");
                }

                renameLog += resultStr;
                parentPathBefore = parentPath;
            }

            return renameIsSuccess;
        }




        internal static bool RunIncrementListenedLastFolders()
        {
            var tmpMessage = "\n\n\n";
            if (Settings.PreviewChanges)
            {
                tmpMessage +=
                    "Сейчас на эран будет выведен ИНКРЕМЕНТ ПАПОК (предварительный просмотр):\n" +
                    "-----------------------------------------------------------------------\n";
            }
            else
            {
                tmpMessage +=
                    "ИНКРЕМЕНТ ПАПОК:\n" +
                    "-----------------------------------------------------------------------\n";
            }

            if (Settings.PreviewChanges)
            {
                Console.WriteLine(tmpMessage);
            }
            else
            {
                Log.Information(tmpMessage);
            }

            if (Settings.PreviewChanges)
            {
                Console.WriteLine("Нажмите любую клавишу...");
                Console.ReadKey();
            }

            tmpMessage = "\n\n" +
                "ЦЕЛЕВОЙ КАТАЛОГ:\n" +
                "---------------\n";

            if (Settings.PreviewChanges)
            {
                Console.WriteLine(tmpMessage);
            }
            else
            {
                Log.Information(tmpMessage);
            }

            var resultBool =
                Methods.IncrementListenedLastFolders(Settings.IntersectedOfTargetNSourceLastFolders, out tmpMessage);

            if (Settings.PreviewChanges)
            {
                Console.WriteLine(tmpMessage);
            }
            else
            {
                if (resultBool)
                {
                    Log.Information(tmpMessage);
                }
                else
                {
                    Log.Error(tmpMessage);
                }
            }

            tmpMessage = "\n\n" +
                "КАТАЛОГ ИСТОЧНИК:\n" +
                "---------------\n";

            if (Settings.PreviewChanges)
            {
                Console.WriteLine(tmpMessage);
            }
            else
            {
                Log.Information(tmpMessage);
            }

            resultBool = resultBool ?
                Methods.IncrementListenedLastFolders(Methods.GetListOfLastFolders(Settings.SourceRoot), out tmpMessage) :
                false;


            if (Settings.PreviewChanges)
            {
                Console.WriteLine(tmpMessage);
            }
            else
            {
                if (resultBool)
                {
                    Log.Information(tmpMessage);
                }
                else
                {
                    Log.Error(tmpMessage);
                }
            }

            tmpMessage = "\n\n\n" + (Settings.PreviewChanges ? "предварительное " : "") +
                "переименование завершено...";

            Console.WriteLine("\n\n\n");
            //Console.ReadKey();

            return resultBool;
        }


    }



}
