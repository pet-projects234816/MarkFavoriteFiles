﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MarkFavoriteFiles;

namespace MarkFavoriteFiles.Config
{

    /// <summary>
    /// Класс для хранения настроек объявленных "внутри"
    /// </summary>
    public static class Settings
    {

        /// <summary>
        /// Множество (перечисление) статусов для тестов: {Success, Error, Warning}
        /// </summary>
        public enum Status { Success, Warning, Error, Indefined }



        /* Инициализация настроек из объекта класса Options */
        /// <summary>
        /// Свойство - корневая директория источник: D:\LikedMusic - берется через инициалзиацию объекта класса Options
        /// </summary>
        public static string SourceRoot { get; set; }

        /// <summary>
        /// Свойство - корневая директория, в которой необходимо отметить файлы: D:\AllMusic - берется через инициалзиацию объекта класса Options
        /// </summary>
        public static string TargetRoot { get; set; }

        /// <summary>
        /// Свойство - символ префикса для переименования файла: понравилось - берется через инициалзиацию объекта класса Options
        /// </summary>
        public static string PrefixLike { get; set; }

        /// <summary>
        /// Свойство - символ префикса для переименования файла: не понравилось - берется через инициалзиацию объекта класса Options
        /// </summary>
        public static string PrefixDislike { get; set; }


        /// <summary>
        /// Свойство - расширение для файлов, которые будут анализироваться - берется через инициалзиацию объекта класса Options ("*.mp3")
        /// </summary>
        public static string FilePattern { get; set; }


        /// <summary>
        /// Свойство - признак: инкрементировать директории с прослушанными файлами
        /// </summary>
        public static bool IncrementDir { get; set; }



        /// <summary>
        /// Свойство - признак: показывать ли полный лог  - берется через инициалзиацию объекта класса Options
        /// </summary>
        public static bool Verbose { get; set; }



        /// <summary>
        /// Свойство - признак того, что неообходимо вывести на экран предполагаемые изменения
        /// </summary>
        public static bool PreviewChanges { get; set; }


        /// <summary>
        /// Свойство - возвращает постфикс для переименования папки для новых файлов
        /// </summary>
        public static string PostfixForNewFilesFolder
        {
            get
            {
                return "_NewFiles";
            }
        }


        public static string FileNameIncrementIsDone
        {
            get
            {
                return "IncrementListenedLastFolders.done";
            }
        }




        /// <summary>
        /// Свойство - возвращает имя папки для новых файлов
        /// </summary>
        public static string FolderForNewFiles
        {
            get
            {
                return SourceRoot.Substring(SourceRoot.LastIndexOf("\\") + 1) + PostfixForNewFilesFolder;
            }
        }





        
        
        /// <summary>
        /// Свойство: Возвращает List(FolderStructure) - пересеченное множество "последних каталогов" в TargetRoot
        /// </summary>
        internal static List<FolderStructure> IntersectedOfTargetNSourceLastFolders
        {
            get; set; 
        }


    }
}
