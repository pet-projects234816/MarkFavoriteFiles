﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using MarkFavoriteFiles.Config;


namespace MarkFavoriteFiles
{
    /// <summary>
    /// Класс: для хранения необходимой информации о каталоге, в котором храняться файлы для переименования
    /// </summary>
    public class FolderStructure
    {
        /// <summary>
        /// Конструктор: инициализация объекта
        /// </summary>
        /// <param name="folderPath"></param>
        public FolderStructure(string folderPath)
        {
            FullPathDirectory = folderPath;
            LastFolder = folderPath.Substring(folderPath.LastIndexOf("\\") + 1);
           
            // _rootDirectory
            if (folderPath.IndexOf(Settings.SourceRoot) == 0)
                _rootDirectory = Settings.SourceRoot;
            else if (folderPath.IndexOf(Settings.TargetRoot) == 0)
                _rootDirectory = Settings.TargetRoot;
            else
                _rootDirectory = String.Empty;


            DiretoryWithoutPathRoot = folderPath.Substring(_rootDirectory.Length);

            // Получить префих до знака "_"
            _prefix = "";
            if (LastFolder.IndexOf("_") != -1) 
            {
                _prefix = LastFolder.Substring(0, LastFolder.IndexOf("_") + 1);

                //Если префик = "_" (_file1.mp3, __file2.mp3), оставляем как есть, иначе убираем первый символ "_"
                if (_prefix != "_")
                {
                    _prefix = _prefix.Substring(0, _prefix.IndexOf("_"));
                }
            }

            // Попытка преобразовать префик из текста в число для формата: "01", "001", "001"
            _stateIncrement = -1;
            if (int.TryParse(_prefix, out _stateIncrement))
            {
                // Чтобы не спутать префикс инкремента и год вводится ограничение до 1900 года
                _stateIncrement = _stateIncrement < 1900 ? _stateIncrement : -1;
            }
            _stateIncrement = (_stateIncrement == 0) ? (_stateIncrement = -1) : _stateIncrement;

        }





        /* ------------------------------------------------------------------------------------------- */
        /*                               Поля и  Свойства                                              */
        /* ------------------------------------------------------------------------------------------- */

        /// <summary>
        /// Поле: Root - стартовая директория хранения каталога (SourceRoot или TargetRoot) - "D:\SourceRoot"
        /// </summary>
        string _rootDirectory;


        /// <summary>
        /// Префих до знака "_"
        /// </summary>
        string _prefix;


        // Состояние префикса-инкремента в значении Int
        int _stateIncrement;

        /// <summary>
        /// Свойство: Путь к директори, без root пути ("\Folder1\Folder2")
        /// </summary>
        public string DiretoryWithoutPathRoot { get; private set; }


        /// <summary>
        /// Свойство: полный путь к диретории
        /// </summary>
        public string FullPathDirectory { get; private set; }

        
        /// <summary>
        /// Свойство: наименование последнего каталога
        /// </summary>
        public string LastFolder { get; private set; }


        /// <summary>
        /// Свойство: количество понравившихся файлов в каталоге
        /// </summary>
        public int QtyLikedFilesInFolder
        {
            get
            {
                return
                    Directory.GetFiles(FullPathDirectory, Settings.PrefixLike + "*" + Settings.FilePattern, SearchOption.TopDirectoryOnly).Count();
            }
        }


        /// <summary>
        /// Свойство: количество непонравившихся файлов в каталоге
        /// </summary>
        public int QtyDislikedFilesInFolder
        {
            get
            {
                return
                    Directory.GetFiles(FullPathDirectory, Settings.PrefixLike + "*" + Settings.FilePattern, SearchOption.TopDirectoryOnly).Count();
            }
        }




        /// <summary>
        /// Свойство: количество каталогов в папке (первого уровня)
        /// </summary>
        public int QtyTopDirInFolder
        {
            get
            {
                return
                    Directory.GetDirectories(FullPathDirectory, "*", SearchOption.TopDirectoryOnly).Count();
            }
        }



        /// <summary>
        /// Свойство: Состояние инкремента для данной папки (количество прослушиваний каталога) : str
        /// </summary>
        public string StateIncrementOfFolderStr  {
            get
            {
                var stateIncrementStr = "";
                if (_stateIncrement > -1 || _prefix == "_" || _prefix.ToUpper() == "X" || _prefix.ToUpper() == "Х")
                {
                    stateIncrementStr = _prefix;
                }

                return stateIncrementStr;

            }
        }





        /// <summary>
        /// Свойство: Состояние инкремента для данной папки (количество прослушиваний каталога; если нет прослушиваний = -1)
        /// </summary>
        public int StateIncrementOfFolderInt
        {
            get
            {
                return _stateIncrement;
            }
        }




        

        /* ------------------------------------------------------------------------------------------- */
        /*                                 Методы                                                      */
        /* ------------------------------------------------------------------------------------------- */


        /// <summary>
        /// Метод: Инкрементировать папку (только для случаев, когда нет вложенных папок)
        /// </summary>
        /// <param name="resultText"> out - результат переименования каталога</param>
        /// <returns>true - успешно, false - не успешно</returns>
        public bool IncrementLastFolder(out string resultText)
        {
            bool renameIsSuccess = true;
            resultText = "";
            
            
            // Инкремент, только если в данной папке нет вложенных каталогов
            if (QtyTopDirInFolder == 0)
            {
                resultText = "\n" + FullPathDirectory.Replace(LastFolder, "") 
                    + "\n       " + LastFolder + " --";
                var numIncrem = 0;

                // Инкремент, если папка до этого уже была инкрементирована
                if (StateIncrementOfFolderInt > -1)
                {
                    numIncrem = StateIncrementOfFolderInt + 1;
                }
                // Иначе 1
                else
                {
                    ++numIncrem;
                }

                // Новое имя папки 
                var newNameLastFolder = numIncrem.ToString() + "_" + LastFolder.Substring(StateIncrementOfFolderStr.Length);
                newNameLastFolder = newNameLastFolder.Replace(numIncrem.ToString() + "__", numIncrem.ToString() + "_");

                // Если в каталоге нет ни одного понравившегося файла
                if (QtyLikedFilesInFolder == 0 & QtyDislikedFilesInFolder > 0)
                {
                    newNameLastFolder = "X_" + newNameLastFolder;
                }


                string newFullPathDirectory = Directory.GetParent(FullPathDirectory) + "\\" + newNameLastFolder;

                try
                {
                    Directory.Move(FullPathDirectory, newFullPathDirectory);

                    // Если это предварительный просмотр - возвращение в исходное состояние
                    if (Settings.PreviewChanges)
                    {
                        Directory.Move(newFullPathDirectory, FullPathDirectory);
                    } 
                }
                catch (Exception ex)
                {
                    resultText += "\n" + ex;
                    renameIsSuccess = false;
                }

                resultText += (renameIsSuccess ? "> " : "x ") + newNameLastFolder;

                return renameIsSuccess;
            }


            return renameIsSuccess;
        }





        /// <summary>
        /// Method: ToDo. Икремент родительской директории и приведение к одной разрадности дочернии папки (..\Folder\2_Folder,11_folder --> ..\02_Folader\02_Folder,11_folder)
        /// </summary>
        /// <param name="resultText"></param>
        /// <returns></returns>
        public bool IncrementAllParentFolders(out string resultText)
        {
            bool renameIsSuccess = true;
            resultText = "";

            var parenDirectory = FullPathDirectory;

            // Работа только с папкой, в которой нет вложенных каталогов
            if (QtyTopDirInFolder == 0)
            {
                resultText += "[" + FullPathDirectory + "]:\n" +
                    "-----------------------------------\n";

                // Работа с родительскими каталогами и их дочерними папками (снизу вверх)
                while (parenDirectory != _rootDirectory)
                {
                    parenDirectory = Directory.GetParent(parenDirectory).ToString();
                    var fdstParent = new FolderStructure(parenDirectory);

                    resultText += "(.." + 
                        ((fdstParent.DiretoryWithoutPathRoot == "") ? "\\" + fdstParent.LastFolder + " (root)" : fdstParent.DiretoryWithoutPathRoot) +
                        ")\n";

                    // Получить список FolderStructure для parenDirectory 
                    List<FolderStructure> listChildFolders = new List<FolderStructure>();

                    var minIncr = -1; // минимальное значение инкримента среди дочерних папок
                    var maxIncr = -1; // максимальное  значение инкримента среди дочерних папок
                    var prefixX = "X";  // 
                    var newNameFolder = "";
                    foreach (var dir in Directory.GetDirectories(parenDirectory, "*", SearchOption.TopDirectoryOnly))
                    {
                        var fdstChild = new FolderStructure(dir);

                        // Если все дочерние папки - отмечены, как не понравившееся, тогда prefixX = X
                        if (!(new[] { "x", "X", "х", "X" }.Contains(fdstChild.StateIncrementOfFolderStr)))
                        {
                            prefixX = "";

                            minIncr = (fdstChild.StateIncrementOfFolderInt < minIncr || minIncr == -1) ?
                                fdstChild.StateIncrementOfFolderInt :
                                minIncr;

                            maxIncr = (fdstChild.StateIncrementOfFolderInt > maxIncr) ?
                                fdstChild.StateIncrementOfFolderInt :
                                maxIncr;
                        }

                        listChildFolders.Add(fdstChild);
                    }

                    // Получить значение разрядности инкермента для дочерних папок
                    var bitIncr = 0;
                    bitIncr = maxIncr > 0 ? (maxIncr.ToString()).Length : bitIncr;

                    resultText += "minIncr=" + minIncr.ToString() +
                        ", maxIncr=" + maxIncr.ToString() +
                        ", bitIncr=" + bitIncr.ToString() + "\n";

                    // Привести значение инкремента в имени дочерних папок к максимальной разрядности
                    foreach (var fdstChild  in listChildFolders)
                    {
                        newNameFolder = fdstChild.LastFolder;
                        var prefix = "";

                        // Если в имени файла разрядность преувеличина...
                        if (fdstChild.StateIncrementOfFolderInt != -1
                            &&
                            fdstChild.StateIncrementOfFolderStr.Length > bitIncr)
                        {
                            newNameFolder = newNameFolder.
                                Substring(fdstChild.StateIncrementOfFolderStr.Length - bitIncr);
                        }


                        if (fdstChild.StateIncrementOfFolderInt != -1)
                        {
                            while ((fdstChild.StateIncrementOfFolderStr.Length + prefix.Length) < bitIncr)
                            {
                                prefix += "0";
                            }
                        }
                        
                        // Рельное переименование дочерних каталогов - анализ на ошибки
                        string newFullPathDirectory = Directory.GetParent(fdstChild.FullPathDirectory) + "\\" + prefix + newNameFolder;
                        try
                        {
                            // Если имя новой папке отличается от оригинала
                            if (fdstChild.FullPathDirectory != newFullPathDirectory)
                            {
                                Directory.Move(fdstChild.FullPathDirectory, newFullPathDirectory);

                                resultText += "\t\t..\\" + fdstChild.LastFolder + " --> " +
                                   prefix + newNameFolder + "\n";
                            }
                            else
                            {
                                resultText += "\t\t..\\" + fdstChild.LastFolder + " --x \n";
                            }

                        }
                        catch (Exception ex)
                        {
                            resultText += "\n" + ex + "\n";
                            renameIsSuccess = false;
                        }
                        
                    }

                    // Инкремент родительского каталога
                    if (parenDirectory == _rootDirectory)
                    {
                        resultText += "..\\root --x ";
                    }
                    else
                    {
                        // Новое имя для переименования
                        var prefix = "";
                        newNameFolder = fdstParent.LastFolder;
                        if (fdstParent.StateIncrementOfFolderStr != "")
                        {
                            if (fdstParent.StateIncrementOfFolderStr == "_")
                            {
                                newNameFolder = newNameFolder.Substring(fdstParent.StateIncrementOfFolderStr.Length);
                            }
                            else
                            {
                                newNameFolder = newNameFolder.Substring(fdstParent.StateIncrementOfFolderStr.Length + 1);
                            }
                        }

                        if (prefixX == "X")
                        {
                            prefix = prefixX + "_";
                        }
                        else
                        {
                            if (minIncr == -1)
                            {
                                prefix = "_";
                            } else
                            {
                                prefix = minIncr.ToString() + "_";
                            }

                        }

                        newNameFolder = prefix + newNameFolder;
                        string newFullPathDirectory = Directory.GetParent(fdstParent.FullPathDirectory) + "\\" + newNameFolder;
                        // ToDo реальное переименование родительского каталога - анализ на ошибки
                        try
                        {
                            if (fdstParent.FullPathDirectory != newFullPathDirectory)
                            {
                                Directory.Move(fdstParent.FullPathDirectory, newFullPathDirectory);
                                resultText += ".." + fdstParent.LastFolder + " --> " + newNameFolder + "\n";
                            }
                            else
                            {
                                resultText += ".." + fdstParent.LastFolder + " --x \n";
                            }
                        }
                        catch (Exception ex)
                        {
                            resultText += "\n" + ex + "\n";
                            renameIsSuccess = false;
                        }
                    }
                    resultText += "\n\n\n";
                }
                resultText = resultText.Substring(0, resultText.Length - 2) + 
                    "----------------------------------";
            }


            return renameIsSuccess;

        }


        public override string ToString()
        {
            return
                FullPathDirectory;
        }

    }
}
