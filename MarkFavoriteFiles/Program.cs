﻿using System;
using CommandLine;
using Serilog;
using MarkFavoriteFiles.Config;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace MarkFavoriteFiles
{
    class Program
    {
        static void Main(string[] args)
        {
            // ToDo - везде в методах resulеLog из строковой переменной переделать в строковый поток


            Options options = new Options();
            var pathFileLog = "..\\logs\\" + DateTime.Today.Year.ToString()  +  "\\inf.log";

            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Verbose()
                .WriteTo.Console(restrictedToMinimumLevel: Serilog.Events.LogEventLevel.Information)
                .WriteTo.RollingFile(pathFileLog, outputTemplate: "{Timestamp:HH:mm:ss.fff} [{Level:u3}] {Message}{NewLine}{Exception}")
                .CreateLogger();

            if (Parser.Default.ParseArguments(args, options))
            {
                // Инициализция настроек, видимых для всего проекта
                Settings.SourceRoot = options.SourceRoot.Trim();
                Settings.TargetRoot = options.TargetRoot.Trim();
                Settings.PrefixLike = options.PrefixLike;
                Settings.PrefixDislike = options.PrefixDislike;
                Settings.FilePattern = options.FilePattern.Trim();
                Settings.IncrementDir = options.IncrementDir;
                Settings.Verbose = options.Verbose;
                Settings.IntersectedOfTargetNSourceLastFolders = Methods.GetIntersectedOfTargetNSourceLastFolders();

                var Tests = new TestsNCheks();

                // Показать значения всех параметров
                if (Settings.Verbose) {
                    Tests.ShowOptionsSettings();
                }

                // Запустить все тесты
                if (Tests.RunAllTestByDefault(Settings.Verbose) == Settings.Status.Error)
                {
                    Console.ReadLine();
                    Environment.Exit(1);
                }

            }
            else
            {
                Console.WriteLine("\n");
                Log.Error("Не верно заданы аргументы....");
                Console.ReadKey();
                Environment.Exit(1);
            }



            // Проверка, производился ли ранее инкремента файлов и папок
            if (File.Exists(Settings.SourceRoot + "\\" + Settings.FileNameIncrementIsDone))
            {
                {
                    Console.WriteLine("\n\n\n");
                    Log.Warning("\nНайден файл \"" + Settings.FileNameIncrementIsDone + "\"\n" +
                        "в дирикетории источнике \"" + Settings.SourceRoot + "\"\n" +
                        "(наличие файла блокирует повторный инкремент)\n\n" +
                        "ИНКРЕМЕНТ отменен.\n\n");
                    Console.WriteLine("Нажмите любую клавишу...");
                    Console.ReadKey();
                    Environment.Exit(2);
                }
            }


            // Создание массива объектов FileStructure для множества файлов в директории источнике
            var filesStructureInSourceRoot = Methods.GetFilesStructureFromPath(Settings.SourceRoot).ToArray();

            // Создание массива объектов FileStructure для множества файлов в целевой директории */
            var filesStructureInTargetRoot = Methods.GetFilesStructureFromPath(Settings.TargetRoot).ToArray();
         


            // Предварительный просмотр
            Settings.PreviewChanges = true;

            Console.WriteLine("\n\n\nСейчас на эран будет выведен предварительный результат переименования:\nНажмите любую клавишу...");
            Console.ReadKey();
            Console.WriteLine("\n\n");

            bool previewResult = Methods.LikeDislikeFiles(filesStructureInSourceRoot, filesStructureInTargetRoot, out string renameLog);
            Console.WriteLine(renameLog);

            var rKey = "";

            if (previewResult)
            {
                Console.WriteLine("\n\nПредварительное переименование прошло успешно.\nПрименить? (Y/N)");
                rKey = Console.ReadKey().Key.ToString();
                while (!(rKey == "Y" || rKey == "N"))
                    {
                        Console.WriteLine("\nВведите допустимый символ ('y','Y','n','N')");
                        rKey = Console.ReadKey().Key.ToString();
                    }
                if (rKey == "N")
                {
                    Console.WriteLine("\n\nПереименование отменено.");
                }
                else
                {
                    Log.Information("\n\n\n\nПЕРЕИМЕНОВАНИЕ ФАЙЛОВ:\n---------------------\n\n");

                    // Переименование файлов
                    Settings.PreviewChanges = false;

                    if (Methods.LikeDislikeFiles(filesStructureInSourceRoot, filesStructureInTargetRoot, out renameLog))
                        Log.Information(renameLog);
                    else
                       Log.Error(renameLog);
                }
            }
            else
            {
                Console.WriteLine("\n\nERROR! Предварительное переименование произошло с ошибками.\nИзменения не сохранены.");
                Console.ReadKey();
                Environment.Exit(1);
            }


            
            
            
            /* Инкремент папок с прослушанной музыкой */
            if (Settings.IncrementDir)
            {
                // Для избежания повторного инкремента (не должно существовать файла "FileNameIncrementIsDone")
                if (!File.Exists(Settings.SourceRoot + "\\" + Settings.FileNameIncrementIsDone))
                {
                    Settings.PreviewChanges = true;
                    previewResult = Methods.RunIncrementListenedLastFolders();

                    if (previewResult)
                    {
                        Console.WriteLine("\n\nПредварительный инкремент папок произошел успешно.\nПрименить? (Y/N)");
                        rKey = Console.ReadKey().Key.ToString();
                        while (!(rKey == "Y" || rKey == "N"))
                        {
                            Console.WriteLine("\nВведите допустимый символ ('y','Y','n','N')");
                            rKey = Console.ReadKey().Key.ToString();
                        }
                        if (rKey == "N")
                        {
                            Console.WriteLine("\n\nИнкремент отменен.");
                        }
                        else
                        {
                            // Инкремент
                            Settings.PreviewChanges = false;
                            if (Methods.RunIncrementListenedLastFolders())
                            {
                                // Создание файла блокирующего инкремент
                                try
                                {
                                    File.Create(Settings.SourceRoot + "\\" + Settings.FileNameIncrementIsDone);
                                }
                                catch (Exception ex)
                                {
                                    Log.Error("\nОшибка при созданиии файла \"" + Settings.FileNameIncrementIsDone + "\"\n" +
                                        "в дирикетории источнике \"" + Settings.SourceRoot + "\"\n" +
                                        "(наличие файла позволяет блокировать повторный инкремент)\n\n" + ex);
                                }

                            }
                            else
                            {
                                Log.Error("\n\nERROR! Ошибка \"реального\" ИНКРЕМЕНТА!\n" +
                                    "что-то пошло не так....");

                                Console.WriteLine();
                                Console.ReadKey();
                                Environment.Exit(1);

                            }
                        }
                    }
                    else
                    {
                        Console.WriteLine("\n\nERROR! Ошибка \"предварительного\" ИНКРЕМЕНТА!\nИзменения не сохранены.");
                        Console.ReadKey();
                        Environment.Exit(1);
                    }

                }
                else
                {
                    Log.Information("\n\nНайден файл \"" + Settings.FileNameIncrementIsDone + "\"\n" +
                        "в дирикетории источнике \"" + Settings.SourceRoot + "\"\n" +
                        "(наличие файла блокирует повторный инкремент)\n\n" +
                        "ИНКРЕМЕНТ ПАПОК отменен.\n\n");
                    Console.WriteLine("Нажмите любую клавишу...");
                    Console.ReadKey();
                    Console.WriteLine("\n\n");

                }

            }




            /// TO DO... ИНКРЕМЕНТ РОДИТЕЛЬСКИХ КАТАЛОГОВ И КОРРЕКТИРООВКА ИХ РАЗРЯДНОСТИ
            Console.WriteLine("\n\nСейчас произойдёт ИНКРЕМЕНТ РОДИТЕЛЬСКИХ КАТАЛОГОВ И КОРРЕКТИРООВКА ИХ РАЗРЯДНОСТИ.\nВыполнить? (Y/N)");
            rKey = Console.ReadKey().Key.ToString();
            while (!(rKey == "Y" || rKey == "N"))
            {
                Console.WriteLine("\nВведите допустимый символ ('y','Y','n','N')");
                rKey = Console.ReadKey().Key.ToString();
            }
            if (rKey == "N")
            {
                Console.WriteLine("\n\nИнкремент отменен.\n");
            }
            else
            {
                // Инкремент родительских каталогов
                var resultLog = "";
                var resultIsSuccess = false;

                var IntersectedCount = Settings.IntersectedOfTargetNSourceLastFolders.Count();
                for (int i = 0; i < IntersectedCount; i++)
                {

                    var fdstIntersected = Settings.IntersectedOfTargetNSourceLastFolders[i];

                    resultIsSuccess = fdstIntersected.IncrementAllParentFolders(out resultLog);
                    if (resultLog != "")
                    {
                        if (resultIsSuccess)
                        {
                            Log.Information("\n\n\n\n\n" + resultLog);
                            Settings.IntersectedOfTargetNSourceLastFolders = Methods.GetIntersectedOfTargetNSourceLastFolders();
                        }
                        else
                        {
                            Log.Error("\n\n\n\n\n" + resultLog);
                            Log.Error("\n\nERROR! Ошибка ИНКРЕМЕНТА РОДИТЕЛЬСКИХ КАТАЛОГОВ!\n" +
                                "что-то пошло не так....");

                            Console.WriteLine("\n");
                            Console.ReadKey();
                            Environment.Exit(1);
                        }
                    }

                }
                


                /*
                foreach (var fdstIntersected in Settings.IntersectedOfTargetNSourceLastFolders)
                {
                    resultIsSuccess = fdstIntersected.IncrementAllParentFolders(out resultLog);
                    if (resultLog != "")
                    {
                        if (resultIsSuccess)
                        {
                            Log.Information("\n\n\n\n\n" + resultLog);
                        }
                        else
                        {
                            Log.Error("\n\n\n\n\n" + resultLog);
                            Log.Error("\n\nERROR! Ошибка ИНКРЕМЕНТА РОДИТЕЛЬСКИХ КАТАЛОГОВ!\n" +
                                "что-то пошло не так....");

                            Console.WriteLine("\n");
                            Console.ReadKey();
                            Environment.Exit(1);
                        }
                    }

                } */

            }


            Console.WriteLine("\n\n\nПроцесс ИНКРЕМЕНТА РОДИТЕЛЬСКИХ КАТАЛОГОВ И КОРРЕКТИРООВКА ИХ РАЗРЯДНОСТИ завершен\n\nНажимите любую клавишу...");
            Console.ReadKey();


        }
    }
}
