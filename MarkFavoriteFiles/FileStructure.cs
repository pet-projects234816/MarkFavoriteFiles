﻿using System;
using System.Collections.Generic;
using System.IO;
using MarkFavoriteFiles.Config;
using System.Text.RegularExpressions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarkFavoriteFiles
{
    /// <summary>
    /// Класс для хранения структуры файла (Имя, полный путь, родительский каталог файла)
    /// </summary>

    class FileStructure
    {
        /* ------------------------------------------------------------------------------------------- */
        /*  Конструкторы                                                                               */
        /* ------------------------------------------------------------------------------------------- */


        /// <summary>
        /// Конструктор: создает только один объект
        /// </summary>
        /// <param name="filePath"> Полный путь к файлу </param>
        public FileStructure(string filePath)
        {
         
            /* Инициализация полей */
            _fileName = Path.GetFileName(filePath);

            // _rootDirectory
            if (filePath.IndexOf(Settings.SourceRoot) == 0)
                _rootDirectory = Settings.SourceRoot;
            else if (filePath.IndexOf(Settings.TargetRoot) == 0)
                _rootDirectory = Settings.TargetRoot;
            else
                _rootDirectory = String.Empty;


            /* Инициализация свойств */
            FullPath = filePath;
            DirectoryFile = Path.GetDirectoryName(filePath);
            DiretoryFileWithoutPathRoot = DirectoryFile.Substring(_rootDirectory.Length);

            ParentDirectory = Directory.GetParent(filePath).Name;
            ParentDir_FileName = ParentDirectory + "\\" + _fileName;


        }


        /* ------------------------------------------------------------------------------------------- */
        /*  Поля и Свойства                                                                            */
        /* ------------------------------------------------------------------------------------------- */

        /// <summary>
        /// Поле: Полное иммя файла, без пути к нему
        /// </summary>
        string _fileName;


        /// <summary>
        /// Поле: Root директория хранения файла (SourceRoot или TargetRoot)
        /// </summary>
        string _rootDirectory;


        /* --------------------------------- */


        /// <summary>
        /// Поле: Директория храниения файла, без root пути 
        /// </summary>
        public string DiretoryFileWithoutPathRoot { get; private set; }


        /// <summary>
        /// Свойстово: Полный путь к файлу
        /// </summary>
        public string FullPath { get; private set; }


        /// <summary>
        /// Поле: Директория расположения фала (полный путь)
        /// </summary>
        public string DirectoryFile { get; private set; }


        /// <summary>
        /// Свойство: Родительский каталог для файла
        /// </summary>
        public string ParentDirectory { get; private set; }



        /// <summary>
        /// Свойство: Родительский каталог + Имя файла
        /// </summary>
        public string ParentDir_FileName { get; private set; }




 



        /* ------------------------------------------------------------------------------------------- */
        /*  Методы                                                                                     */
        /* ------------------------------------------------------------------------------------------- */

        /// <summary>
        /// Метод - переименование файла (изменение структуры всего объекта)
        /// </summary>
        /// <param name="isLike">true - файл понравился, false - не поравился</param>
        /// <param name="resultText">результат переименования в текстовом виде</param>
        /// <returns>true - выполнено успешно (даже, если имя файла не изменилось), false - ошибка при переименовании</returns>
        public bool RenameFileByPrefix (bool isLike, out string resultText)
        {
            bool renameIsSuccess = true;
            resultText = "\n.." + DiretoryFileWithoutPathRoot 
                 + "\n       " +_fileName;
            string fileNameRename = "";

            /* Убрать префиксы в файле источнике, если они уже были присвоены ранее */
            if (_fileName.IndexOf(Settings.PrefixLike) == 0)
                fileNameRename = _fileName.Substring(Settings.PrefixLike.Length, _fileName.Length - Settings.PrefixLike.Length);

            if (_fileName.IndexOf(Settings.PrefixDislike) == 0)
                fileNameRename = _fileName.Substring(Settings.PrefixDislike.Length, _fileName.Length - Settings.PrefixDislike.Length);

            fileNameRename = (fileNameRename == "") ? _fileName : fileNameRename;

            /* Определиться с результирующим именем файла переименования */
            if (isLike)
                fileNameRename = Settings.PrefixLike + fileNameRename;
            else
                fileNameRename = Settings.PrefixDislike + fileNameRename;


            /* Переименование файла */
            if (fileNameRename != _fileName || _fileName != Settings.FileNameIncrementIsDone)
                try
                {
                    File.Move(FullPath, DirectoryFile + "\\" + fileNameRename);

                    if (Settings.PreviewChanges)
                        File.Move(DirectoryFile + "\\" + fileNameRename, FullPath);
                    else
                    {
                        FullPath = DirectoryFile + "\\" + fileNameRename;
                        _fileName = fileNameRename;
                        ParentDir_FileName = ParentDirectory + "\\" + fileNameRename;
                    }

                    resultText += " --> " + fileNameRename + (Settings.PreviewChanges ? String.Empty : " (success)");


                }
                catch (Exception ex)
                {
                    renameIsSuccess = false;
                    resultText += " --x ...failed.\n" + ex.ToString();
                }
            else
                if (Settings.PreviewChanges) 
                    resultText = String.Empty;
                else
                    resultText += " --[" + (isLike ? Settings.PrefixLike : Settings.PrefixDislike).Trim() +  "] ...без изменений (success)";


            return renameIsSuccess;

        }


        /// <summary>
        /// Метод - копирует файлы (новые) в папку %SourceRoot%\_NewFiles\..
        /// </summary>
        /// <param name="resultText"> текст результата копирования </param>
        /// <returns>true/false - успешность выполнения</returns>
        public bool CopyNewFiles(out string resultText)
        {
            bool copyIsSuccess = true;
            resultText = String.Empty;

            /* Убрать префиксы в файле источнике, если они уже были присвоены ранее */
            string fileNameRename = "";

            if (_fileName.IndexOf(Settings.PrefixLike) == 0)
                fileNameRename = _fileName.Substring(Settings.PrefixLike.Length, _fileName.Length - Settings.PrefixLike.Length);

            fileNameRename = (fileNameRename == "") ? _fileName : fileNameRename;

            var fullPathToCopy = _rootDirectory + Settings.PostfixForNewFilesFolder + DiretoryFileWithoutPathRoot + "\\" + Settings.PrefixLike + fileNameRename;
            var directoryPachToCopy = _rootDirectory + Settings.PostfixForNewFilesFolder + DiretoryFileWithoutPathRoot;

            resultText = "..\\" + ParentDir_FileName;


            // Если данный файл уже копировался в "новую папку" - выход из метода
            if (File.Exists(fullPathToCopy))
            {
                if (Settings.PreviewChanges)
                    resultText = String.Empty;
                else
                    resultText += "\n    (copy to) --> ..\\"
                        + Settings.FolderForNewFiles
                        + DiretoryFileWithoutPathRoot + "\\ --[+] ...без изменений (success)" ;
                return copyIsSuccess;
            }



            if (!Settings.PreviewChanges)
            {
                try 
                {

                    // Создание каталога
                    Directory.CreateDirectory(directoryPachToCopy);
                    // Копирование файлов
                    File.Copy(FullPath, fullPathToCopy, false);
                }
                catch (Exception ex)
                {
                    copyIsSuccess = false;
                    resultText += " --x\n" + ex.ToString();
                }
            }

            resultText += "\n    (copy to) --> ..\\"
                + Settings.FolderForNewFiles
                + DiretoryFileWithoutPathRoot + "\\" + Settings.PrefixLike + fileNameRename; ;
            return copyIsSuccess;

        }





        public override string ToString()
        {
            return
                "СТРУКТУРА ФАЙЛА." +
                "\nПолный путь: " + FullPath +
                "\nРодительская директория: " + ParentDirectory +
                "\nФайл в директории: " + ParentDir_FileName;

        }
    }
}
